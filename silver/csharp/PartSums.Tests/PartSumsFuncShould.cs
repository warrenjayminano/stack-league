using System;
using Xunit;

namespace PartSums.Tests
{
    public class PartSumsFuncShould
    {
        [Fact]
        public void ReturnListOfSummation()
        {
            Assert.Equal(new int[] { 0 }, PartSumsFunc.GetSums(new int[] { }));
            Assert.Equal(new int[] { 20, 20, 19, 16, 10, 0  }, PartSumsFunc.GetSums(new int[] { 0, 1, 3, 6, 10  }));
            Assert.Equal(new int[] { 21, 20, 18, 15, 11, 6, 0 }, PartSumsFunc.GetSums(new int[] { 1, 2, 3, 4, 5, 6 }));
        }
    }
}
