﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PartSums
{
    public class PartSumsFunc
    {

        public static int[] GetSums(int[] ls)
        {
            if (ls == null)
            {
                throw new ArgumentNullException(nameof(ls));
            }

            List<int> response = new List<int>();

            List<int> numberList = new List<int>(ls);
            // Add zero offset
            numberList.Add(0);

            try
            {
                int numberListCount = numberList.Count();
                for (int i = 0; i < numberListCount; i++)
                {
                    response.Add(numberList.Sum());
                    numberList.RemoveAt(0);
                }
            }
            catch (OverflowException)
            {
                throw new OverflowException("Overflow reached for sum of params ls");
            }

            return response.ToArray();
        }
    }
}
