﻿using System;
using System.Collections.Generic;

namespace Wave
{
    public class WaveConverter
    {
        public static IEnumerable<string> GetWaveString(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return new List<string>();
            }

            var response = new List<string>();

            var strArr = s.ToCharArray();

            for (int i = 0; i <= strArr.Length-1; i++)
            {
                if (char.IsWhiteSpace(strArr[i]))
                {
                    continue;
                } else
                {
                    char[] formattedStr = s.ToLower().ToCharArray();
                    formattedStr.SetValue(char.ToUpper(strArr[i]), i);
                    response.Add(new string(formattedStr));
                }
            }

            return response;
        }
    }
}
