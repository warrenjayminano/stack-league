﻿using System;
using System.Collections.Generic;

namespace PrimeGaps
{
    public class PrimeGap
    {
        /// <summary>
        /// Finds first pair of number having prime gap defined between range of numbers provided
        /// </summary>
        /// <param name="g"></param>
        /// <param name="m"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static long[] Gap(int g, long m, long n)
        {
            List<long> response = null;

            int gap;
            long fromNumber, toNumber;

            try
            {

                checked
                {
                    gap = g;
                    fromNumber = m;
                    toNumber = n;
                }

                if (gap <= 0 || fromNumber < 0 || toNumber < 0)
                {
                    return null;
                }

                long firstPrime = -1;
                long secondPrime = -1;

                for (long i = fromNumber; i <= toNumber; i++)
                {
                    if (IsPrime(i)){
                        if(firstPrime == -1)
                        {
                            firstPrime = i;
                        } else { 
                            if(secondPrime == -1)
                            {
                                secondPrime = i;
                            }
                        }

                        // Evaluate prime gap
                        if(firstPrime != -1 && secondPrime != -1)
                        {
                            if(getPrimeGap(firstPrime, secondPrime) == gap)
                            {
                                response = new List<long>();
                                response.Add(firstPrime);
                                response.Add(secondPrime);

                                break;
                            } else
                            {
                                firstPrime = -1;
                                secondPrime = -1;
                            }
                        }
                    }
                }
            }
            catch (OverflowException)
            {
                throw new OverflowException("Provided value out of range");
            }
            catch (ArgumentNullException)
            {
                throw;
            }
            catch (Exception) { throw; }

            return response?.ToArray();
        }

        /// <summary>
        /// Finds the prime gap between two given numbers
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        /// <returns></returns>
        private static int getPrimeGap(long num1, long num2)
        {
            return (int)(num2 - num1);
        }

        /// <summary>
        /// Check if number is a prime
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static bool IsPrime(long n)
        {
            long number;
            try
            {
                checked
                {
                    number = n;
                }
            }
            catch (OverflowException)
            {
                throw new OverflowException();
            }

            int i;
            for (i = 2; i <= number - 1; i++)
            {
                if (number % i == 0)
                {
                    return false;
                }
            }
            if (i == number)
            {
                return true;
            }
            return false;
        }
    }
}
