﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace WildWest
{
    public class MovementManager
    {
        public static string[] getDirection(String[] directions)
        {
            if(directions == null)
            {
                throw new ArgumentNullException(nameof(directions));
            }

            if(directions == Array.Empty<string>())
            {
                throw new ArgumentOutOfRangeException(nameof(directions));
            }
            //CODE HERE
            return formatListOfMoves(getOptimizedMoveList(getValidEntries(directions))).ToArray();
        }

        /// <summary>
        /// Formats list of moves
        /// </summary>
        /// <param name="listOfMoves"></param>
        /// <returns>Formatted list of moves</returns>
        private static IEnumerable<string> formatListOfMoves(IEnumerable<string> listOfMoves)
        {
            List<string> formattedMoves = new List<string>();

            foreach(string move in listOfMoves)
            {
                formattedMoves.Add(move.ToUpper());
            }

            return formattedMoves;
        }

        /// <summary>
        /// Evaluate list of moves removing unnecessary move sets
        /// </summary>
        /// <param name="listOfMoves"></param>
        /// <returns>List of optimized moves</returns>
        private static IEnumerable<string> getOptimizedMoveList(IEnumerable<string> listOfMoves)
        {
            List<string> optimizedMoves = new List<string>();

            if(listOfMoves.Count() > 1)
            {
                optimizedMoves.Add(listOfMoves.ElementAt(0));

                for (int i = 1; i < listOfMoves.Count(); i++)
                {
                    if(isMovementNecessary(listOfMoves.ElementAt(i), listOfMoves.ElementAt(i-1)))
                    {
                        optimizedMoves.Add(listOfMoves.ElementAt(i));
                    }
                }
            } 
            else
            {
                optimizedMoves = listOfMoves.ToList();
            }

            return optimizedMoves;
        }

        /// <summary>
        /// Removes invalid entries from the list
        /// </summary>
        /// <param name="listOfMoves"></param>
        /// <returns>List of valid entries</returns>
        private static IEnumerable<string> getValidEntries(IEnumerable<string> listOfMoves)
        {
            List<string> validEntries = new List<string>();
            foreach (string value in listOfMoves)
            {
                if (checkValidCasing(value))
                {
                    validEntries.Add(value);
                }
            }

            return validEntries;
        }

        /// <summary>
        /// Evaluates if current move is unnecassary
        /// </summary>
        /// <param name="currentMove"></param>
        /// <param name="previousMove"></param>
        /// <returns>Returns true if current move is opposite of previous move, otherwise false</returns>
        private static bool isMovementNecessary(string currentMove, string previousMove)
        {   if(currentMove.Equals("EAST", StringComparison.InvariantCultureIgnoreCase) || currentMove.Equals("WEST", StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }
            return !(previousMove.Equals(getOppositeMove(currentMove), StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Retrieves the opposite move based on move provided
        /// </summary>
        /// <param name="move"></param>
        /// <returns>String opposite move</returns>
        private static string getOppositeMove(string move)
        {
            string oppositeMove = move.ToUpper() switch
            {
                "NORTH" => "SOUTH",
                "SOUTH" => "NORTH",
                "EAST" => "WEST",
                "WEST" => "EAST",
                _ => string.Empty,
            };
            return oppositeMove;
        }

        /// <summary>
        /// Validates input matches either all uppercase or all lowercase only
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Returns true if all characters on input matches casing.</returns>
        private static bool checkValidCasing(string input)
        {
            return checkValueMatchesRegex(input, @"^[a-z]+$|^[A-Z]+$");
        }

        /// <summary>
        /// Checks if input matches provided regex format
        /// </summary>
        /// <param name="input"></param>
        /// <param name="regex"></param>
        /// <returns>Returns true value matches regex pattern</returns>
        private static bool checkValueMatchesRegex(string input, string regex) => Regex.IsMatch(input, regex);
    }
}
