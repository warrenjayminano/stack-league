using System;
using Xunit;

namespace BirthdayInvites.Tests
{
    public class InviteManagerShould
    {
        private readonly InviteManager _sut;

        public InviteManagerShould()
        {
        }

        [Fact]
        public void ThrowExceptionOnInvalidParameter()
        {
            Assert.Throws<ArgumentNullException>(() => InviteManager.getFormattedInviteList(null));
            Assert.Throws<ArgumentNullException>(() => InviteManager.getFormattedInviteList(string.Empty));
            Assert.Throws<ArgumentNullException>(() => InviteManager.getFormattedInviteList(" "));
            Assert.Throws<FormatException>(() => InviteManager.getFormattedInviteList("Ramon:Hamorabi,Datu:Puti"));
            Assert.Throws<FormatException>(() => InviteManager.getFormattedInviteList("Ramon,Hamorabi;Datu,Puti"));
            Assert.Throws<FormatException>(() => InviteManager.getFormattedInviteList("A:Doe,A:Doe"));
            Assert.Throws<ArgumentOutOfRangeException>(() => InviteManager.getFormattedInviteList("Ramon,Hamorabi;Datu:Puti"));
            Assert.Throws<ArgumentOutOfRangeException>(() => InviteManager.getFormattedInviteList("Ramon:Hamorabi;Datu:Puti;Mawn:Ten:Dew;"));
            Assert.Throws<ArgumentOutOfRangeException>(() => InviteManager.getFormattedInviteList("R4mon: H4morabi; D@tu: Put!; Barney: Dadaynasur; Betty: Tobull; Hugh: Tobull; Mawnten: Dew; Bokid: Dew"));
            
        }

        [Fact]
        public void SortNamesAlphabeticallyByLastNameThenByFirstName()
        {
            Assert.Equal("(DOE, A)", InviteManager.getFormattedInviteList("A:Doe"));
            Assert.Equal("(DOE, A)(DOE, A)", InviteManager.getFormattedInviteList("A:Doe;A:Doe"));
            Assert.Equal("(DEW, BOKID)(DEW, MAWNTEN)(TOBULL, BETTY)(TOBULL, HUGH)", InviteManager.getFormattedInviteList("Betty: Tobull; Hugh: Tobull; Mawnten: Dew; Bokid: Dew"));
            Assert.Equal("(DADAYNASUR, BARNEY)(DEW, BOKID)(DEW, MAWNTEN)(HAMORABI, RAMON)(PUTI, DATU)(TOBULL, BETTY)(TOBULL, HUGH)", InviteManager.getFormattedInviteList("Ramon: Hamorabi; Datu: Puti; Barney: Dadaynasur; Betty: Tobull; Hugh: Tobull; Mawnten: Dew; Bokid: Dew"));
        }
    }
}
