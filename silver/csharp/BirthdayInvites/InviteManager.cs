﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BirthdayInvites
{
    public class InviteManager
    {
        public static string invites(string s) => (getFormattedInviteList(s));
        private static string getFormattedInviteList(string input)
        {
            string result = string.Empty;

            if (validateInput(input, ':', ';'))
            {
                result = formatDisplay(sortList(parseInput(getFormattedCasing(input), ":", ";")));
            }

            return result;
        }

        /// <summary>
        /// Validates strToValidate provided 
        /// </summary>
        /// <param name="strToValidate">raw string value to be validated</param>
        /// <returns>Returns true if validation passes. false otherwise.</returns>
        /// <returns>Returns ArgumentNullException when strToValidate is null</returns>
        /// /// <returns>Returns ArgumentOutOfRangeException when strToValidate is empty or whitespace</returns>
        private static bool validateInput(string strToValidate, char separator, char delimeter)
        {
            string regexDefault = @"^[a-zA-Z :;]*$";

            if (strToValidate == null)
            {
                throw new ArgumentNullException(nameof(strToValidate));
            }

            if (strToValidate.Equals(string.Empty) || string.IsNullOrWhiteSpace(strToValidate))
            {
                throw new ArgumentNullException(nameof(strToValidate));
            }


            // Check if separator exists
            if (!checkValueExists(strToValidate, separator)) { throw new FormatException(nameof(strToValidate)); } else
            {
                if(isDelimiterExpected(strToValidate, separator))
                {
                    // Check if delimeter exists
                    if (!checkValueExists(strToValidate, delimeter)) { throw new FormatException("Separator is missing"); }
                }
            }

            // Check if value contains valid characters only
            if(!checkValueMatchesRegex(strToValidate, regexDefault)) { throw new ArgumentOutOfRangeException(nameof(strToValidate)); }
            return true;
        }

        /// <summary>
        /// Checks for specified value on provided string input
        /// </summary>
        /// <param name="input">string to search</param>
        /// <param name="value">char to search for</param>
        /// <returns>True if value found, otherwise false</returns>
        private static bool checkValueExists(string input, char value) => input.Contains(value);
        /// <summary>
        /// Checks for specified value on provided string input
        /// </summary>
        /// <param name="input">string to search</param>
        /// <param name="value">string to search for</param>
        /// <returns>True if value found, otherwise false</returns>
        private static bool checkValueExists(string input, string value) => input.Contains(value);
        /// <summary>
        /// Checks for value matches regex for validating input
        /// </summary>
        /// <param name="input">string to search</param>
        /// <param name="regex">regex pattern to match</param>
        /// <returns>True if match, otherwise false</returns>
        private static bool checkValueMatchesRegex(string input, string regex) => Regex.IsMatch(input, regex);
        /// <summary>
        /// Counts occurrence of indicator having more than 1 occurrence reqires delimeter on string input
        /// </summary>
        /// <param name="input"></param>
        /// <param name="indicator"></param>
        /// <returns>Returns true if more than one instance of indicator found on string input</returns>
        private static bool isDelimiterExpected(string input, char indicator) => input.Count(x => x == indicator) > 1;

        /// <summary>
        /// Converts input to uppercase
        /// </summary>
        /// <param name="input"></param>
        /// <returns>input string all in uppercase</returns>
        private static string getFormattedCasing(string input) => input.ToUpper();
        /// <summary>
        /// Parses string input into IEnumerable<Tuple<string, string>> format. Parses string to list of names then parses each name into subset of string,string tuple representing name1 and name2
        /// </summary>
        /// <param name="strToParse"></param>
        /// <param name="separator"></param>
        /// <param name="delimeter"></param>
        /// <returns>List of names</returns>
        private static IEnumerable<Tuple<string, string>> parseInput(string strToParse, string separator = ":", string delimeter = ";")
        {
            List<Tuple<string, string>> namesTupleList = null;

            if (strToParse == null)
            {
                throw new ArgumentNullException(nameof(strToParse));
            }

            if(strToParse.Length == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(strToParse));
            }

            List<string> rawNamesList;
            try
            {
                rawNamesList = new List<string>(strToParse.Split(delimeter));

                if(rawNamesList != null && rawNamesList.Count > 0) { namesTupleList = new List<Tuple<string, string>>(); }
                
            }
            catch (Exception)
            {
                throw;
            }

            try
            {
                foreach (string fullName in rawNamesList)
                {
                    namesTupleList.Add(new Tuple<string, string>(
                        fullName.Substring(0, fullName.IndexOf(separator)).Trim(),
                        fullName.Substring(fullName.IndexOf(separator) + 1).Trim()));
                }
            }
            catch (FormatException ex)
            {
                throw new FormatException($"{nameof(strToParse)}, {ex.Message}", ex);
            }
            catch (Exception)
            {
                throw;
            }

            return namesTupleList;
        }

        /// <summary>
        /// Sorts list of tuple from item 2 then by item 1
        /// </summary>
        /// <param name="listToSort">list to sort</param>
        /// <returns>Sorted list of tuple</returns>
        private static IEnumerable<Tuple<string, string>> sortList(IEnumerable<Tuple<string, string>> listToSort) => listToSort.OrderBy(x => x.Item2).ThenBy(x => x.Item1).ToList();

        /// <summary>
        /// Formats list into comma separated name 1 and name two each entry being enclosed with parenthesis (name1, name2)
        /// </summary>
        /// <param name="listToFormat"></param>
        /// <returns>formatted string of invites</returns>
        private static string formatDisplay(IEnumerable<Tuple<string, string>> listToFormat)
        {
            StringBuilder sb = new StringBuilder();
            
            foreach(Tuple<string,string> item in listToFormat)
            {
                sb.Append($"({item.Item2}, {item.Item1})");
            }

            return sb.ToString();
        }
    }
}
