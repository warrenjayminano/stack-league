﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FindMissingStudent
{
    public class FinderTool
    {
        private static int findMissingNumber(int[] numbers, int beginsWith = 1)
        {
            if(numbers == null)
            {
                throw new ArgumentNullException(nameof(numbers));
            }
            
            if(numbers.Length == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(numbers));
            }

            int expectedNumber = beginsWith;

            try
            {
                List<int> numbersList = new List<int>(numbers);

                numbersList.Sort();

                

                foreach (int number in numbersList)
                {
                    if (number != expectedNumber) { return expectedNumber; }
                    expectedNumber++;
                }
            }
            catch { throw; }
            
            // interpret missing number as the last number on expected list
            return expectedNumber;
        }
    }
}
