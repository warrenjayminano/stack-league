﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace FractionDecomposition
{
    public class FractionManager
    {
        public static string Decompose(string nrStr, string drStr)
        {
            if(string.IsNullOrWhiteSpace(nrStr) || string.IsNullOrWhiteSpace(drStr))
            {
                throw new ArgumentNullException("Invalid values provided");
            }

            //if(!Regex.IsMatch(@"\d+", nrStr) || !Regex.IsMatch(@"\d+", drStr))
            //{
            //    throw new ArgumentOutOfRangeException("Invalid values provided");
            //}

            string response = "[]";
            int numerator, denominator;

            var fraction = IsNumeric(nrStr, drStr);
            if (string.IsNullOrWhiteSpace(fraction))
            {
                response  = "[]";
            }
            else
            {
                var symbolIndex = fraction.IndexOf('/');
                numerator = Convert.ToInt32(fraction.Substring(0, symbolIndex));
                denominator = Convert.ToInt32(fraction.Substring(symbolIndex + 1));

                if (numerator <= 0 || denominator <= 0)
                {
                    response = "[]";
                }
                else if (numerator % denominator == 0)
                {
                    response = $"[{numerator / denominator}]";
                }
                else if (denominator % numerator == 0)
                {
                    response = $"[1/{denominator / numerator}]";
                }
                else
                {
                    response = getFractionParts(numerator, denominator);
                }


            }

            return response;
        }
        public static string IsNumeric(string numberOne, string numberTwo)
        {
            string response = string.Empty;
            try
            {
                checked { 
                    int numOne, numTwo;

                    if(Int32.TryParse(numberOne, out numOne) && Int32.TryParse(numberTwo, out numTwo))
                    {
                        response = $"{numberOne}/{numberTwo}";
                    } 
                    else
                    {
                        throw new ArgumentException();
                    }
                }
            }
            catch (OverflowException)
            {
                throw new OverflowException("Values truncated");
            }
            catch (Exception) { throw; }

            return response;
        }

        private static string getFractionParts(int numerator, int denominator)
        {
            string result = string.Empty;
            result = formatFractionParts(getFractionPartRecursive(numerator, denominator, new List<Tuple<int, int>>()));
            return result;
        }

        
        /// <summary>
        /// Retrieve least denominator for each fraction part recursive process
        /// </summary>
        /// <param name="numerator"></param>
        /// <param name="denominator"></param>
        /// <param name="fractions"></param>
        /// <returns></returns>
        private static List<Tuple<int, int>> getFractionPartRecursive(int numerator, int denominator, List<Tuple<int, int>> fractions)
        {
            //List<Tuple<int, int>> fractionList = fractions;
            if(denominator % numerator == 0)
            {
                fractions.Add(new Tuple<int, int>(1, denominator / numerator));
            } 
            else if(numerator % denominator == 0)
            {
                fractions.Add(new Tuple<int, int>(denominator/ numerator, 1));
            } 
            else if (numerator > denominator)
            {
                fractions.Add(new Tuple<int, int>(numerator/ denominator, 1));
                getFractionPartRecursive(numerator % denominator, denominator, fractions);
            }
            else
            {
                int remaining = (denominator / numerator + 1);
                fractions.Add(new Tuple<int, int>(1, remaining));
                getFractionPartRecursive(numerator * remaining - denominator, denominator * remaining, fractions);
            }

            return fractions;
        }

        /// <summary>
        /// Formats list of fractions into string format
        /// </summary>
        /// <param name="fractions"></param>
        /// <returns></returns>
        private static string formatFractionParts(List<Tuple<int, int>> fractions)
        {
            StringBuilder sb = new StringBuilder();

            List<string> fractionStringList = new List<string>();

            if(fractions.Count > 0)
            {
                foreach(Tuple<int, int> fraction in fractions)
                {
                    if(fraction.Item2 == 1)
                    {
                        fractionStringList.Add($"{fraction.Item1}");
                    } else {
                        fractionStringList.Add($"{fraction.Item1}/{fraction.Item2}");
                    }
                }

                sb.AppendJoin(", ", fractionStringList);
                sb.Insert(0, '[').Append(']');
            }
            else
            {
                sb.Append("[]");
            }

            return sb.ToString();
        }
    }
}
