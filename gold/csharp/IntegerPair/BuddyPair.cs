﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntegerPair
{
    public class BuddyPair
    {

        public static string getBuddyPair(long start, long limit)
        {
            string response = "Nothing";

            long startInclusive, limitInclusive, firstNum, secondNum;

            if(start <= 0 || limit <= 0 || start >= limit)
            {
                throw new ArgumentOutOfRangeException("params start and/or limit with invalid value(s)");
            }

            try
            {
                checked
                {
                    startInclusive = start;
                    limitInclusive = limit;
                }
            }
            catch (OverflowException)
            {
                throw new OverflowException();
            }

            try
            {
                for (long i = startInclusive; i <= limitInclusive; i++)
                {
                    if (isPrime(i)) continue;

                    firstNum = i;
                    secondNum = getProperDivisors(i).Sum() - 1;

                    if(firstNum == getProperDivisors(secondNum).Sum() - 1)
                    {
                        response = $"({firstNum} {secondNum})";
                        break;
                    }
                }
            }
            catch (OverflowException)
            {
                throw new OverflowException();
            }
            catch (Exception) { throw; }

            return response;
        }
        private static IEnumerable<long> getProperDivisors(long number)
        {
            //if (number == 1) return null;

            List<long> divisors = new List<long>();
            divisors.Add(1);

            for(int i = 2; i<Math.Sqrt(number); i++)
            {
                if((number % i) == 0)
                {
                    divisors.Add(i);
                    if(i != number / i) { divisors.Add(number / i); }
                }
            }
            return divisors;
        }
        private static bool isPrime(long n)
        {
            long number;
            try
            {
                checked
                {
                    number = n;
                }
            }
            catch (OverflowException)
            {
                throw new OverflowException();
            }

            int i;
            for (i = 2; i <= number - 1; i++)
            {
                if (number % i == 0)
                {
                    return false;
                }
            }
            if (i == number)
            {
                return true;
            }
            return false;
        }
    }
}
