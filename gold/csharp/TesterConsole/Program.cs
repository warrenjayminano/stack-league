﻿using FractionDecomposition;
using IntegerPair;
using PyrammidScammas;
using System;

namespace TesterConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //var x = BuddyPair.GetBuddyPair(3, long.MaxValue);
            //var y = BuddyPair.GetBuddyPair(48, 50);

            //Person top = new Person();
            //Person level1 = new Person();
            //Person level2a = new Person();
            //Person level2b = new Person();
            //level1.Recruit(level2a);
            //level1.Recruit(level2b);
            //top.Recruit(level1);
            //top.Recruit(new Person());
            //top.Recruit(new Person());
            //var g = top.GetLoserCount();

            var z = FractionManager.Decompose("2", "3");
            var x = FractionManager.Decompose("21", "23");
            var c = FractionManager.Decompose("12", "4");
            var v = FractionManager.Decompose("3", "4");
            var b = FractionManager.Decompose("0", "2");
            var n = FractionManager.Decompose("9", "10");
            var m = FractionManager.Decompose("125", "100");
        }
    }
}
