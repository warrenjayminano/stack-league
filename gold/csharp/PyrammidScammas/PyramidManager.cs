﻿using System;
using System.Collections.Generic;

namespace PyrammidScammas
{
    public class PyramidManager
    {

    }

    public class Person
    {
        List<Person> recruits = new List<Person>();

        public void Recruit(Person otherPerson)
        {
            this.recruits.Add(otherPerson);
        }

        public int GetLoserCount()
        {
            return countNoRecruitsRecursive(this, 0);
        }

        private int countNoRecruitsRecursive(Person person, int count)
        {
            int currentCount = count;
            if(person.recruits.Count == 0)
            {
                currentCount++;
            } 
            else
            {
                foreach(Person recruitedPerson in person.recruits)
                {
                    currentCount += countNoRecruitsRecursive(recruitedPerson, count);
                }
            }

            return currentCount;
        }

        private bool hasRecruits(Person person) => person.recruits.Count > 0;
    }
}
