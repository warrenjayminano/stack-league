﻿using System;

namespace Overpopulation
{
    public class PopulationMonitor
    {
        private static int calculateDurationGrowth(int startAmount, double? increaseRate, int additionalAmountValue, long targetAmount)
        {
            if(startAmount <= 0 || targetAmount <= 0)
            {
                throw new ArgumentOutOfRangeException("Invalid startAmount value");
            }

            if (!increaseRate.HasValue && additionalAmountValue <= 0)
            {
                throw new ArgumentOutOfRangeException("Invalid increaseRate and additionalAmount values");
            }

            if(increaseRate.HasValue && increaseRate <= 0.00)
            {
                throw new ArgumentOutOfRangeException("Invalid increaseRate value");
            }

            int duration = 0;

            try
            {
                checked
                {
                    int additionalAmount = additionalAmountValue;

                    int currentAmount = startAmount;

                    while (currentAmount < targetAmount)
                    {
                        // Add percentage increase
                        if (increaseRate.HasValue) { currentAmount += Convert.ToInt32((currentAmount * (increaseRate.Value / 100))); }

                        // Add fixed increase
                        if (additionalAmount > 0) { currentAmount += additionalAmount; }

                        duration++;
                    }
                }

            }
            catch (OverflowException)
            {
                throw new OverflowException();
            }
            catch (Exception) { throw; }

            return duration;
        }
    }
}
