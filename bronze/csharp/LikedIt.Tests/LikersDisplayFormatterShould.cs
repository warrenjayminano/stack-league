using System;
using System.Collections.Generic;
using Xunit;

namespace LikedIt.Tests
{
    public class LikersDisplayFormatterShould
    {
        private readonly LikersDisplayFormatter _sut;

        public LikersDisplayFormatterShould(){
            _sut = new LikersDisplayFormatter();
        }


        [Fact]
        public void ThrowArgumentNullExceptionWhenArrayIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => _sut.GetLikersFormattedDisplay(null));
        }

        [Fact]
        public void AddConjunctionWhenAtLeastTwoLikers()
        {
            string[] likers = {"John Doe", "Joe Mama", "Papa John"};
            var conjunction = "and";
            Assert.Contains(conjunction, _sut.GetLikersFormattedDisplay(likers));
        }

        [Fact]
        public void NoConjunctionWhenSingleOrEmpty()
        {
            string[] likers = {"John Doe"};
            var conjunction = "and";
            Assert.DoesNotContain(conjunction, _sut.GetLikersFormattedDisplay(likers));
        }

        [Fact]
        public void NotAddDelimiterNorConjunctionOnLessThanTwoValidEntries()
        {
            var delimiter = ",";
            var conjunction = "and";

            Assert.DoesNotContain(delimiter, _sut.GetLikersFormattedDisplay(new string[] { "John Doe" }));
            Assert.DoesNotContain(conjunction, _sut.GetLikersFormattedDisplay(new string[] { "John Doe" }));

            Assert.DoesNotContain(delimiter, _sut.GetLikersFormattedDisplay(new string[] { "" }));
            Assert.DoesNotContain(conjunction, _sut.GetLikersFormattedDisplay(new string[] { "" }));

            Assert.DoesNotContain(delimiter, _sut.GetLikersFormattedDisplay(new string[] { " " }));
            Assert.DoesNotContain(conjunction, _sut.GetLikersFormattedDisplay(new string[] { " " }));
        }

        [Fact]
        public void AddDelimiterAndConjunctionOnTwoOrMoreValidEntries()
        {
            var delimiter = ",";
            var conjunction = "and";

            Assert.Contains(conjunction, _sut.GetLikersFormattedDisplay(new string[] { "John Doe", "Joe Mama" }));
            Assert.Contains(conjunction, _sut.GetLikersFormattedDisplay(new string[] { "John Doe", "", "Papa John" }));

            Assert.Contains(delimiter, _sut.GetLikersFormattedDisplay(new string[] { "John Doe", "Joe Mama", "Papa John" }));
            Assert.Contains(conjunction, _sut.GetLikersFormattedDisplay(new string[] { "John Doe", "Joe Mama", "Papa John" }));

        }

        [Fact]
        public void DisplayFormattedTextForLessThanTwoEntries()
        {
            var delimiter = ",";
            var conjunction = "and";

            Assert.Equal($"Nobody likes this post", _sut.GetLikersFormattedDisplay(new string[] { "" }));
            Assert.Equal($"Nobody likes this post", _sut.GetLikersFormattedDisplay(new string[] { null }));
            Assert.Equal($"Nobody likes this post", _sut.GetLikersFormattedDisplay(new string[] { " " }));

            Assert.Equal($"John likes this post", _sut.GetLikersFormattedDisplay(new string[] { "John" }));
            Assert.Equal($"John Doe likes this post", _sut.GetLikersFormattedDisplay(new string[] { "John Doe" }));
        }

        [Fact]
        public void DisplayFormattedTextForTwoOrMoreEntries()
        {
            var delimiter = ",";
            var conjunction = "and";

            var twoLikers = new string[] { "John Doe", "Joe" };
            var threeLikers = new string[] { "John Doe", "Joe Mama", "Papa John" };
            var moreThanThreeLikers = new string[] { "John", "Joe Mama", "Papa John", "Mama Bear" };

            Assert.Equal($"{twoLikers[0]} {conjunction} {twoLikers[1]} like this post", _sut.GetLikersFormattedDisplay(twoLikers));

            Assert.Equal($"{threeLikers[0]}{delimiter} {threeLikers[1]} {conjunction} {threeLikers[2]} like this post", _sut.GetLikersFormattedDisplay(threeLikers));
            Assert.Equal($"{threeLikers[0]}{delimiter} {threeLikers[1]} {conjunction} {threeLikers[2]} like this post", _sut.GetLikersFormattedDisplay(threeLikers));

            Assert.Equal($"{moreThanThreeLikers[0]}{delimiter} {moreThanThreeLikers[1]} {conjunction} {moreThanThreeLikers.Length - 2} others like this post", _sut.GetLikersFormattedDisplay(moreThanThreeLikers));

        }

    }
}
