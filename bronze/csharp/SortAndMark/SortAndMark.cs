﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortAndMark
{
    public class SortAndMark
    {
        /// <summary>
        /// Sorts a given list of strings alphabetically and returns the first entry formatted with each character separated with specified separator
        /// </summary>
        /// <param name="listOfStrings">list of strings to process</param>
        /// <param name="separator">separator to be used to separate characters of top string</param>
        /// <returns>top string from the sorted list formatted</returns>
        public string GetSortedMarkedEntry(string[] listOfStrings, string separator = "---")
        {
            return formatString(getStringToFormat(sortAlphabetically(listOfStrings)), separator);
        }

        /// <summary>
        /// Sorts array of strings alphabetically, case sensitive
        /// </summary>
        /// <param name="strArrToSort">collection of string to sort</param>
        /// <returns>sorted list of strings</returns>
        private IEnumerable<string> sortAlphabetically(string[] strArrToSort)
        {
            if (strArrToSort == null)
            {
                throw new ArgumentNullException(nameof(strArrToSort));
            }

            if (strArrToSort.Length == 0 || strArrToSort == Array.Empty<string>())
            {
                throw new ArgumentException("Unable to sort empty collection provided", nameof(strArrToSort));
            }

            List<string> sortedList = new(strArrToSort);

            if (sortedList.Count > 1) { sortedList.Sort(StringComparer.Ordinal); }

            return sortedList;
        }

        /// <summary>
        /// Formats a given string to per character separated with defined separator
        /// </summary>
        /// <param name="stringToFormat">string to format</param>
        /// <param name="separator">string to use to separate characters, default ---</param>
        /// <returns>formatted string</returns>
        private string formatString(string stringToFormat, string separator = "---")
        {
            return string.Join(separator, stringToFormat.ToCharArray());
        }

        /// <summary>
        /// Retrieves the first entry on the list
        /// </summary>
        /// <param name="stringList">list of strings</param>
        /// <returns>Returns the first entry on the list</returns>
        private string getStringToFormat(IEnumerable<string> stringList)
        {
            return stringList.ElementAt(0);
        }
    }
}
