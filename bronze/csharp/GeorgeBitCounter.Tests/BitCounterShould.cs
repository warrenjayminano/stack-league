using System;
using Xunit;

namespace GeorgeBitCounter.Tests
{
    public class BitCounterShould
    {
        private readonly BitCounter _sut;

        public BitCounterShould()
        {
            _sut = new BitCounter();
        }
        [Fact]
        public void CountCharacterFoundOnInteger()
        {
            char bitToCount = '1';
            Assert.Equal(0, _sut.CountBitOnInt(0, bitToCount));
            Assert.Equal(1, _sut.CountBitOnInt(4, bitToCount));
            Assert.Equal(2, _sut.CountBitOnInt(10, bitToCount));
            Assert.Equal(8, _sut.CountBitOnInt(255, bitToCount));
        }

        [Fact]
        public void ReturnZeroOnInvalidCharToCountParam()
        {
            char bitToCount = 'x';
            Assert.Equal(0, _sut.CountBitOnInt(0, bitToCount));
            Assert.Equal(0, _sut.CountBitOnInt(4, bitToCount));
            Assert.Equal(0, _sut.CountBitOnInt(10, bitToCount));
            Assert.Equal(0, _sut.CountBitOnInt(255, bitToCount));
        }

        [Fact]
        public void ThrowArgumentOutOfRangeExceptionOnNegativeParam()
        {
            char bitToCount = '1';
            Assert.Throws<ArgumentOutOfRangeException>(() => _sut.CountBitOnInt(-1, bitToCount));
            Assert.Throws<ArgumentOutOfRangeException>(() => _sut.CountBitOnInt(int.MinValue, bitToCount));
        }
    }
}
