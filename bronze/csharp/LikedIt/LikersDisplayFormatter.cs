﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace LikedIt
{
    public class LikersDisplayFormatter
    {
        /// <summary>
        /// Formats list of likers into defined format and template
        /// </summary>
        /// <param name="likersList">list of names</param>
        /// <param name="delimiter">delimiter text to use</param>
        /// <param name="conjunction">conjunction text to use</param>
        /// <param name="addConjunctionOnCount">count of names to add conjunction</param>
        /// <param name="maxNamesDisplayedCount">max count of names displayed</param>
        /// <returns>Formatted text to be displayed on label for likes. Returns ArgumentNullException when likersList is null</returns>
        public string GetLikersFormattedDisplay(string[] likersList, string delimiter = ", ", string conjunction = "and", int addConjunctionOnCount = 2, int maxNamesDisplayedCount = 3)
        {
            StringBuilder formattedTextDisplay = new StringBuilder();
            try
            {
                var cleanedLikersList = GetValidatedLikersList(likersList);

                if (cleanedLikersList.Length == 1)
                {
                    formattedTextDisplay.Append(cleanedLikersList[0]);
                    formattedTextDisplay.Append(getTextToAppend(DisplayLimitType.Single));
                } else if(cleanedLikersList.Length > 1) // process multiple entries
                {
                    formattedTextDisplay.Append(formatMultipleLikersEntries(cleanedLikersList, delimiter, conjunction, addConjunctionOnCount, maxNamesDisplayedCount));

                    if(cleanedLikersList.Length <= maxNamesDisplayedCount)
                    {
                        formattedTextDisplay.Append(getTextToAppend(DisplayLimitType.MultipleWithinLimit));
                    } 
                    else
                    {
                        formattedTextDisplay.Append(getTextToAppend(DisplayLimitType.MultipleMoreThanLimit));
                    }
                } else
                {
                    formattedTextDisplay.Append(getTextToAppend(DisplayLimitType.None));
                }

                return formattedTextDisplay.ToString();
            }
            catch(ArgumentNullException)
            {
                if (likersList == null)
                {
                    throw new ArgumentNullException(nameof(likersList));
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(likersList));
            }
            catch (ArrayTypeMismatchException)
            {
                throw new ArrayTypeMismatchException(nameof(likersList));
            }
            catch (Exception ex)
            {
                throw new Exception($"Unhandled exception encountered. Please contact the administrator. Error: {ex.Message}");
            }
            finally
            {
                formattedTextDisplay.Clear();
                formattedTextDisplay.Append(getTextToAppend(DisplayLimitType.None));
            }
            return formattedTextDisplay.ToString();
        }

        /// <summary>
        /// Returns a list containing non-empty, non-whitespece, alpha only 
        /// </summary>
        /// <param name="likersList"></param>
        /// <returns>Returns validated and cleaned list. Returns ArgumentNullException when rawEntriesList is null</returns>
        private string[] GetValidatedLikersList(string[] likersList)
        {
            try
            {
                //return removeWhitespaceAndEmptyEntries(removeNonAlphaOrSpaceContainingEntries(likersList));
                return removeWhitespaceAndEmptyEntries(likersList);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Removes empty and null items on list with each entry having any leading or trailing spaces removed
        /// </summary>
        /// <param name="rawEntriesList">List of raw names</param>
        /// <returns>Non-empty entries with removed trailing and leading spaces. </returns>

        private string[] removeWhitespaceAndEmptyEntries(string[] rawEntriesList)
        {
            List<string> cleanedList;

            if (rawEntriesList == null)
            {
                throw new ArgumentNullException(nameof(rawEntriesList));
            }

            if(rawEntriesList.Length == 0)
            {
                return Array.Empty<string>();
            }

            cleanedList = new List<string>();

            foreach (string element in rawEntriesList)
            {
                if (!string.IsNullOrWhiteSpace(element))
                {
                    cleanedList.Add(element.Trim());
                }
            }

            return cleanedList.ToArray<string>();
        }

        /// <summary>
        /// Removes entries containin non-alpha non-space characters
        /// </summary>
        /// <param name="rawEntriesList"></param>
        /// <returns></returns>
        private string[] removeNonAlphaOrSpaceContainingEntries(string[] rawEntriesList)
        {
            List<string> cleanedList;

            if (rawEntriesList == null)
            {
                throw new ArgumentNullException(nameof(rawEntriesList));
            }

            if (rawEntriesList.Length == 0)
            {
                return Array.Empty<string>();
            }

            cleanedList = new List<string>();

            foreach (string element in rawEntriesList)
            {
                if (Regex.IsMatch(element, @"^[a-zA-Z0 ]*$"))
                {
                    cleanedList.Add(element.Trim());
                }
            }

            return cleanedList.ToArray<string>();
        }

        /// <summary>
        /// Returns pre formatted line to append based on count of likers case with respect to limit range
        /// </summary>
        /// <param name="displayLimitType">display type to be evaluated for formatted line</param>
        /// <returns></returns>
        private string getTextToAppend(DisplayLimitType displayLimitType)
        {
            string textToAppend;
            switch (displayLimitType)
            {
                case DisplayLimitType.None: 
                    textToAppend = "Nobody likes this post"; break;
                case DisplayLimitType.Single:
                    textToAppend = " likes this post"; break;
                case DisplayLimitType.MultipleWithinLimit:
                    textToAppend = " like this post"; break;
                case DisplayLimitType.MultipleMoreThanLimit:
                    textToAppend = " others like this post"; break;
                default: 
                    textToAppend = "Nobody likes this post"; break;
            }

            return textToAppend;
        }

        /// <summary>
        /// Formats multiple entries with delimers and conjunctions, appends conjunction based on addConjunctionOnCount, appends display name plus remaining likers count based on defined maxNamesDisplayedCount 
        /// </summary>
        /// <param name="likers">list of names</param>
        /// <param name="delimiter">delimiter text to use</param>
        /// <param name="conjunction">conjunction text to use</param>
        /// <param name="addConjunctionOnCount">count of names to add conjunction</param>
        /// <param name="maxNamesDisplayedCount">max count of names displayed</param>
        /// <returns></returns>
        private string formatMultipleLikersEntries(string[] likers, string delimiter, string conjunction, int addConjunctionOnCount = 2, int maxNamesDisplayedCount = 3)
        {
            StringBuilder formattedEntries = new StringBuilder(); ;

            if (likers.Length >= addConjunctionOnCount && likers.Length < maxNamesDisplayedCount)
            {
                formattedEntries.AppendJoin($" {conjunction} ", likers);
            } 
            else if(likers.Length >= addConjunctionOnCount && likers.Length == maxNamesDisplayedCount)
            {
                formattedEntries.Append(string.Join(delimiter, likers, 0, (likers.Length - 1)));
                formattedEntries.Append($" {conjunction} {likers[likers.Length - 1]}");
            } 
            else if (likers.Length >= addConjunctionOnCount && likers.Length > maxNamesDisplayedCount)
            {
                formattedEntries.Append(string.Join(delimiter, likers, 0, (likers.Length - 2)));
                formattedEntries.Append($" {conjunction} {likers.Length - 2}");
            }

            return formattedEntries.ToString();
        }

        /// <summary>
        /// Defined enums for display type of names
        /// </summary>
        private enum DisplayLimitType
        {
            None,
            Single,
            MultipleWithinLimit,
            MultipleMoreThanLimit
        }
    }
}
