using System;
using Xunit;

namespace SortAndMark.Tests
{
    public class SortAndMarkShould
    {
        private readonly SortAndMark _sut;

        public SortAndMarkShould()
        {
            _sut = new SortAndMark();
        }
        [Fact]
        public void ThrowExceptionsOnInvalidParamatersProvided()
        {
            Assert.Throws<ArgumentNullException>(() => _sut.GetSortedMarkedEntry(null));
            Assert.Throws<ArgumentException>(() => _sut.GetSortedMarkedEntry(Array.Empty<string>()));
            Assert.Throws<ArgumentException>(() => _sut.GetSortedMarkedEntry(new string[0]));
        }

        [Fact]
        public void ReturnTopStringFormatted()
        {
            string separator = "---";
            Assert.Equal("a", _sut.GetSortedMarkedEntry(new string[] { "d", "p", "ldwa", "a", "b" }, separator));
            Assert.Equal("a---l---p---h---a---1", _sut.GetSortedMarkedEntry(new string[] { "beta2", "alpha1" }, separator));
            Assert.Equal("1", _sut.GetSortedMarkedEntry(new string[] { "1151", "61316", "1" }, separator));
        }

        [Fact]
        public void ReturnTopStringFormattedCaseSensitive()
        {
            string separator = "---";
            Assert.Equal("a---a---A---A", _sut.GetSortedMarkedEntry(new string[] { "aaBB", "aaAa", "aaab", "aaAA"}, separator));
        }

    }
}
