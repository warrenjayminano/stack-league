﻿using System;
using System.Collections.Generic;

namespace GeorgeBitCounter
{
    public class BitCounter
    {
        /// <summary>
        /// Count specified bit on non-negative integer value converted to binary
        /// </summary>
        /// <param name="value">non-negative integer value to count bit when converted to binary</param>
        /// <param name="bit">char / bit to count on binary converted integer value</param>
        /// <returns></returns>
        public int CountBitOnInt(int value, char bit = '1')
        {
            return getCharCount(getBinaryValue(value), bit);
        }
        /// <summary>
        /// Converts integer to string binary representation
        /// </summary>
        /// <param name="value">non-negative</param>
        /// <returns>string binary conversion of non-negative integer value.</returns>
        /// <returns>ArgumentOutOfRangeException if value was not provided or negative value provided</returns>
        private string getBinaryValue(int value)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }

            return Convert.ToString(value, 2);
        }

        /// <summary>
        /// Counts occurrence of specified character on given string
        /// </summary>
        /// <param name="value">string to be searched</param>
        /// <param name="charToCount">character to search for</param>
        /// <returns>int count of occurrence</returns>
        private int getCharCount(string value, char charToCount)
        {
            return (new List<char>(value.ToCharArray()).FindAll(x => x == charToCount)).Count;
        }
    }
}
